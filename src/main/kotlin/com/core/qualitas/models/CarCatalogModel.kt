package com.core.qualitas.models

data class CarCatalogModel (

    var id: String = "",

    var text: String = ""

)