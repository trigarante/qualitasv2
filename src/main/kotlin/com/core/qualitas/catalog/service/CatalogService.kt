package com.core.qualitas.catalog.service

import com.core.qualitas.catalog.cars.repository.CarsRepository
import com.core.qualitas.catalog.private.repository.PrivateRepository
import com.core.qualitas.models.CarCatalogModel
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CatalogService: CatalogInterface{
    @Autowired
    private lateinit var cars: CarsRepository

    @Autowired
    private lateinit var private: PrivateRepository

    override fun catalog(type: String): MutableList<CarCatalogModel> {
        when(type){
            "Private"->{
                return private.getMarcas()
            }else->{
                return cars.getMarcas()
            }
        }
    }

    override fun catalog(brand: String, type: String): MutableList<CarCatalogModel> {
        when(type) {
            "Private" -> {
                return private.getModelos(brand)
            }
            else -> {
                return cars.getModelos(brand)
            }
        }
    }

    override fun catalog(brand: String, model: String, type: String): MutableList<CarCatalogModel> {
        when(type) {
            "Private" -> {
                return private.getSubMarcas(brand, model)
            }
            else -> {
                return cars.getSubMarcas(brand, model)
            }
        }
    }

    override fun catalog(brand: String, model: String, subBrand: String, type: String): MutableList<CarCatalogModel> {
        when(type) {
            "Private" -> {

            }
            else -> {

            }
        }
    }

}