package com.core.qualitas.catalog.service

import com.core.qualitas.models.CarCatalogModel

abstract interface CatalogInterface{
    abstract fun catalog(type: String): MutableList<CarCatalogModel>
    abstract fun catalog(brand: String,type: String): MutableList<CarCatalogModel>
    abstract fun catalog(brand: String,model: String,type: String): MutableList<CarCatalogModel>
    abstract fun catalog(brand: String,model: String, subBrand: String,type: String): MutableList<CarCatalogModel>
}
