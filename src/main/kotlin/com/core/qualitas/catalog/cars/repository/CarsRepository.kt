package com.core.qualitas.catalog.cars.repository

import com.core.qualitas.models.CarCatalogModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface CarsRepository: JpaRepository<CarCatalogModel, String> {
    @Query("SELECT CONCAT( 'm', ROW_NUMBER() OVER ( ORDER BY ( CASE WHEN o.ordenMarcas IS NULL THEN 1 ELSE 0 END ), o.ordenMarcas, m.marca )) AS id, m.marca AS text FROM Concentrado c Join Marcas m on c.idMarca = m.id LEFT JOIN Orden o ON m.marca = o.marca GROUP BY m.marca, o.ordenMarcas", nativeQuery = true)
    fun getMarcas():MutableList<CarCatalogModel>

    @Query("select distinct c.idModelo as id , mo.Modelo as text from `Concentrado` c join Marcas m on c.idMarca = m.id join Modelos mo on c.idModelo = mo.id where m.Marca = :marca order by mo.Modelo  DESC",nativeQuery = true)
    fun getModelos(@Param("marca")marca: String):MutableList<CarCatalogModel>

    @Query("SELECT CONCAT( 'm', ROW_NUMBER() OVER ( ORDER BY ( CASE WHEN o.ordenSubmarcas IS NULL THEN 1 ELSE 0 END ), o.ordenSubmarcas, s.SubMarcas )) id, s.SubMarcas AS text FROM Concentrado c join Marcas m on m.id = c.idMarca join Modelos mo on mo.id = c.idModelo join SubMarcas s on s.id = c.idSubmarca LEFT JOIN Orden o ON m.marca = o.marca AND s.SubMarcas = o.submarca WHERE m.marca = :marca AND modelo = :modelo GROUP BY s.SubMarcas, o.ordenSubmarcas",nativeQuery = true)
    fun getSubMarcas(@Param("marca")marca:String, @Param("modelo")modelo:String):MutableList<CarCatalogModel>

    @Query("SELECT DISTINCT Clv.Clave  as id, D.Descripcion  as text from Concentrado C join Marcas M on C.idMarca = M.id join SubMarcas S on C.idSubMarca = S.id Join Modelos Md on C.idModelo = Md.id join Claves Clv on C.idClave = Clv.id join Descripciones D on C.idDescripcion = D.id WHERE M.MARCA = :marca and  Md.Modelo = :modelo and S.SubMarcas =:subMarca",nativeQuery = true)
    fun getDescripciones(@Param("marca") marca : String, @Param("modelo") modelo : String, @Param("subMarca") subMarca : String) : MutableList<CarCatalogModel>
}