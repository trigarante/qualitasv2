package com.core.qualitas

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class QualitasApplication

fun main(args: Array<String>) {
	runApplication<QualitasApplication>(*args)
}
